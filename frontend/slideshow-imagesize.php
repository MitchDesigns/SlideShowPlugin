<?php
function slideshow_plugin_image_setup()
{
    add_theme_support('post-thumbnails');
    add_image_size('md-slideshow', 602, 339, true);
}

add_action('after_setup_theme', 'slideshow_plugin_image_setup');