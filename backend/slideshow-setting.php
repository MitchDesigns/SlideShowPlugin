<?php
/*
 * Create Slideshow Setting Menu
 */
function setup_theme_slideshow_menu()
{
    add_menu_page('Theme settings', 'Mitchdesigns Slideshow', 'manage_options',
        'mitchdesigns-slideshow-setting', 'mitchdesigns_slideshow_setting_page', 'dashicons-images-alt2');

}

add_action("admin_menu", "setup_theme_slideshow_menu");

/*
 * Create Slideshow Setting Menu
 */
function mitchdesigns_slideshow_setting_page()
{
    ?>
    <script>
        $(function () {
            $("#mdSlideshowImagesList").sortable();
            $("#mdSlideshowImagesList").disableSelection();
        });
    </script>
    <div class="wrap">
        <h2>Mitchdesigns Slideshow Setting</h2>

        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking
            at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
            as opposed .</p>

        <div class="md-upload-box">
            <?php $max_upload = (int)(ini_get('upload_max_filesize')); ?>
            <span class="action-upload-panel">
                <a href="javascript:;" id="mdSlideshowUpload" class="browser button button-hero">Add Images</a>
            </span>
        </div>
        <?php
        $slideshow_image_list = get_option('slideshow_image_list');
        ?>
        <div class="md-slideshow-images">
            <div class="wp-filter">
                <span>Mitchdesign Slideshow Images</span>
                <a href="javascript:;" id="mdSlideshowSave" class="button-primary button-large">Save Changes</a>
            </div>
            <ul id="mdSlideshowImagesList">
                <?php
                foreach ($slideshow_image_list as $image_id):
                    $image_obj = wp_get_attachment_image_src($image_id, 'thumbnail');
                    $image_src = $image_obj[0];
                    ?>
                    <li data-imageid=" <?php echo $image_id; ?> ">
                        <div class="remove-image">
                            <a href="javascript:;" class="md-slideshow-remove">
                                <i class="fa fa-trash-o fa-lg"></i>
                            </a>
                        </div>
                        <img src="<?php echo $image_src; ?>">
                    </li>

                <?php
                endforeach;
                ?>
            </ul>
        </div>

    </div>
<?php
}