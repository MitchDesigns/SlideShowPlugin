<?php

// Upload Images to Slideshow
function upload_slideshow_images()
{
    $all_attachments = $_POST['image_id_array'];
    $new_attachments = $_POST['uploaded_images'];
    if (!empty($new_attachments) && !empty($all_attachments)) {
        $new_attachments_markup = '';
        foreach ($new_attachments as $attachment) {
            $new_attachment_obj = wp_get_attachment_image_src($attachment, 'thumbnail');
            $new_attachment_src = $new_attachment_obj[0];
            $new_attachments_markup .= '<li data-imageid="' . $attachment . '"><div class="remove-image"><a href="javascript:;" class="md-slideshow-remove"><i class="fa fa-trash-o fa-lg"></i></a></div><img src="' . $new_attachment_src . '"/></li>';
        }
        $update_slideshow_images = update_option('slideshow_image_list', $all_attachments);
        if (!$update_slideshow_images) {

            echo json_encode(array(
                'error' => array(
                    'msg' => 'Sorry , Try again later !',
                ),
            ));
        } else {

            echo json_encode(array(
                'success' => array(
                    'new_images' => $new_attachments_markup,
                ),
            ));

        }
    } else {
        echo json_encode(array(
            'error' => array(
                'msg' => 'Sorry , You must select photos to upload',
            ),
        ));
    }


    die();
}

add_action('wp_ajax_upload_slideshow_images_action', 'upload_slideshow_images');
add_action('wp_ajax_nopriv_upload_slideshow_images_action', 'upload_slideshow_images');

// Save Images Changes to Slideshow
function save_slideshow_images_changes()
{
    $all_attachments = $_POST['image_id_array'];
    if (!empty($all_attachments)) {

        $update_slideshow_images = update_option('slideshow_image_list', $all_attachments);
        if (!$update_slideshow_images) {

            echo json_encode(array(
                'error' => array(
                    'msg' => 'Sorry , Try again later !',
                ),
            ));
        } else {

            echo json_encode(array(
                'success' => array(
                    'msg' => 'Success',
                ),
            ));

        }
    } else {
        echo json_encode(array(
            'error' => array(
                'msg' => 'Sorry , You must select photos to upload',
            ),
        ));
    }


    die();
}

add_action('wp_ajax_save_slideshow_images_changes_action', 'save_slideshow_images_changes');
add_action('wp_ajax_nopriv_save_slideshow_images_changes_action', 'save_slideshow_images_changes');
?>
