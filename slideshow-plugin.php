<?php
/*
Plugin Name: Slideshow Plugin
Plugin URI: http://www.123.com
Description: Plugin for making slideshow and manage it from backend, You can use it into frontend using Short Code [mdslideshow]
Author: Karim Omar
Version: 1.0
Author URI: http://www.karimomar.me/
*/

// Require All Backend Dependencies
$dependencies = array(
    'backend/slideshow-setting.php',
    'backend/slideshow-actions.php',
    'frontend/slideshow-shortcode.php',
    'frontend/slideshow-imagesize.php',
    'includes/backend/enqueue/enqueue.php',
    'includes/frontend/enqueue/enqueue.php'

);

foreach ($dependencies as $dependency):
    include $dependency;
endforeach;