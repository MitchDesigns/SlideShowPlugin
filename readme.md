# Slide Show Plugin 
##### Wordpress Slide Show Plugin

### About Plugin 
It is a Wordpress Plugin for Slide Show Component, Through this plugin you can manage slide show from backend (Admin Dashboard) and (Add, Remove and Re-order) images through Ajax based panel, Also you can use it into frontend (Wordpress Front Page or Theme) through Wordpress Short Code " [mdslideshow] " .

### Installation Guide 
**Clone** this repo into folder **Named** " slideshow-plugin " located into website plugins folder, Go to website admin dashboard > Plugins and **Activate** Plugin .

## Enjoy

#### About Author 
- Name : Karim Omar
- Email : karim.omar92@gmail.com
- Website : [KarimOmar.me](http://karimomar.me)