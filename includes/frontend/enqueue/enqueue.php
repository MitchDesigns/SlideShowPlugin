<?php
/*
 * Scripts and Styles enqueue Frontend
 */
function frontend_custom_enqueue()
{
    //Register Style and Script
    wp_register_style('light_slider_css', plugins_url() . '/slideshow-plugin/includes/assets/css/lightSlider.css');
    wp_register_script('light_slider_js', plugins_url() . '/slideshow-plugin/includes/assets/js/jquery.lightSlider.js', array('jquery'), '1.0', false);

    //Enqueue Styles and Scripts
    wp_enqueue_style('light_slider_css');
    wp_enqueue_script('light_slider_js');

}

add_action('wp_enqueue_scripts', 'frontend_custom_enqueue');