<?php
/*
 * Scripts and Styles enqueue admin dashboard
 */
function admin_dashboard_custom_enqueue()
{
    //Register Style and Script
    wp_register_style('admin_dashboard_style', plugins_url() . '/slideshow-plugin/includes/assets/css/admin-style.css');
    wp_register_script('admin_dashboard_script', plugins_url() . '/slideshow-plugin/includes/assets/js/admin-script.js', array('jquery'), '1.0', true);
    wp_register_script('jquery-1.10', '//code.jquery.com/jquery-1.10.2.js', false);
    wp_register_script('jquery-1.11', '//code.jquery.com/ui/1.11.3/jquery-ui.js', false);

    // Register Font Awesome
    wp_register_style('fontawesome-css', plugins_url() . '/slideshow-plugin/includes/assets/css/font-awesome.min.css', array(), '3.2.0');

    //Enqueue Styles and Scripts
    wp_enqueue_style('admin_dashboard_style');
    wp_enqueue_script('admin_dashboard_script');
    wp_enqueue_script('jquery-1.10');
    wp_enqueue_script('jquery-1.11');
    wp_enqueue_style('fontawesome-css');

    //Enqueue Media Functions
    wp_enqueue_media();

    // Localize Ajax URL in
    wp_localize_script('admin_dashboard_script', 'wp_ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
}

add_action('admin_enqueue_scripts', 'admin_dashboard_custom_enqueue');